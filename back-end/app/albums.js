const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const permit = require('../middleware/permit');
const auth = require('../middleware/auth');
const path = require('path');
const Album = require('../models/Album');
const config = require('../config');
const Track = require('../models/Track');
const upload = require('../multer').albumsImage;

const router = express.Router();

router.get("/", async (req, res) => {
    let albums;
    try {
        if (req.query.artist) {
            albums = await Album.find({artist: req.query.artist}).sort('releaseDate');
                albums = await Promise.all(albums.map(async album =>{
                    let count=0;
                    await Track.aggregate([{$match: {album: album._id }}, {$group: { _id: album._id , count: { $sum: 1 }}
                    }], function(err, trackCount) {
                        if(trackCount.length>0) count=trackCount[0].count;
                    });
                    return {...album._doc,count};
                }));
        } else {
            albums = await Album.find();
        }
        res.send(albums);
    } catch (error) {
        return res.status(400).send(error);
    }

});
router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if(id){
        try {
            const albums = await Album.findOne({_id: id}).populate('artist');
            if (!albums) {
                return res.status(404).send({message: "Album not found"});
            }
            return res.send(albums);
        }
        catch(error){
            return res.status(400).send(error);
        }
    }
});

router.post('/', [auth, upload.single('coverImage')], async (req, res) => {
    const user = req.user;
    const newAlbum = req.body;
    if (req.file) {
        newAlbum.coverImage = req.file.filename;
    }
    newAlbum.user = user._id
    try {
        const album = new Album(newAlbum);
        await album.save();
        res.send(album);
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const album = await Album.findOne({_id: id});
        if(!album){
            return res.status(404).send("No this album");
        }
        await Album.deleteOne({_id: id});
        await Track.deleteMany({album:id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.post('/publish/:id',[auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const album = await Album.findOne({_id: id});
        album.isPublished = true;
        await album.save();
        res.send(album);
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;