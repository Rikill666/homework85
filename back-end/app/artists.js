const express = require('express');
const nanoid = require('nanoid');
const multer = require('multer');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const path = require('path');
const Artist = require('../models/Artist');
const Album = require('../models/Album');
const Track = require('../models/Track');
const config = require('../config');
const upload = require('../multer').artistsPhoto;

const router = express.Router();

router.get("/", async (req, res) => {
    try {
        const artists = await Artist.find();
        res.send(artists);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get("/:id", async (req, res) => {
    const id = req.params.id;
    if (id) {
        try {
            const artist = await Artist.findOne({_id: id});
            if (!artist) {
                return res.status(404).send({message: "Task not found"});
            }
            return res.send(artist);
        } catch (error) {
            return res.status(400).send(error);
        }
    }
});

router.post('/',[auth, upload.single('image')], async (req, res) => {
    const user = req.user;
    const newArtist = req.body;
    if (req.file) {
        newArtist.image = req.file.filename;
    }
    newArtist.user = user._id;
    try {
        const artist = new Artist(newArtist);
        await artist.save();
        res.send(artist);
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const artist = await Artist.findOne({_id: id});
        if(!artist){
            return res.status(404).send("No this artist");
        }
        await Artist.deleteOne({_id: id});
        const albums = await Album.find({artist: id});
        await Album.deleteMany({artist:id});
        if(albums.length>0){
            await Promise.all(albums.map(async a => {
               await Track.deleteMany({album: a._id});
            }));
        }
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.post('/publish/:id',[auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const artist = await Artist.findOne({_id: id});
        artist.isPublished = true;
        await artist.save();
        res.send(artist);
    } catch (error) {
        return res.status(400).send(error);
    }
});

module.exports = router;