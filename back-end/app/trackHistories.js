const express = require('express');
const TrackHistory = require('../models/TrackHistory');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const router = express.Router();


router.post('/', auth, async (req, res) => {
    const user = req.user;
    const trackId = req.query.track;


    const track = await Track.findOne({_id: trackId});
    if (!track) {
        return res.status(400).send({error: "Wrong id track"});
    }
    try {
        const trackHistory = new TrackHistory({user: user._id, track: trackId, dateTime: new Date()});
        await trackHistory.save();
        return res.send(trackHistory);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.get('/', auth, async (req, res) => {
    const user = req.user;
    try {
        let trackHistories = await TrackHistory.find({user: user._id}).populate({
            path: 'track',
            populate: {path: 'album', populate: {path: 'artist'}}
        }).sort('-dateTime');
        trackHistories = trackHistories.map(history => {
            return {
                id:history._id,
                trackTitle: history.track.title,
                albumTitle: history.track.album.title,
                dateTime: history.dateTime,
                artistName: history.track.album.artist.name
            };
        });
        return res.send(trackHistories);
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;