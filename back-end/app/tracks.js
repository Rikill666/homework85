const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');
const Track = require('../models/Track');
const Album = require('../models/Album');

const router = express.Router();

router.get("/", async (req, res) => {
    let tracks;
    try{
        if(req.query.artist){
            const albums = await Album.find({artist:req.query.artist});
            tracks = (await Promise.all(albums.map(a => {
                return Track.find({album: a._id});
            }))).flat();
        }
        else if(req.query.album){
            tracks = await Track.find({album:req.query.album}).sort('trackNumber');
        }
        else{
            tracks = await Track.find();
        }
        res.send(tracks);
    }
    catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/',auth, async (req, res) => {
    const user = req.user;
    const newTrack = req.body;
    newTrack.user = user._id;
    try {
        const track = new Track(req.body);
        await track.save();
        res.send(track);
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.delete('/:id', [auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const track = await Track.findOne({_id: id});
        if(!track){
            return res.status(404).send("No this album");
        }
        await Track.deleteOne({_id: id});
        res.send("ok");
    } catch (error) {
        return res.status(400).send(error);
    }
});
router.post('/publish/:id',[auth, permit('admin')], async (req, res) => {
    const id = req.params.id;
    try {
        const track = await Track.findOne({_id: id});
        track.isPublished = true;
        await track.save();
        res.send(track);
    } catch (error) {
        return res.status(400).send(error);
    }
});
module.exports = router;