const path = require('path');
const fs = require('fs');

const express = require('express');
const axios = require('axios');
const nanoid = require('nanoid');
const bcrypt = require("bcrypt");

const config = require('../config');
const User = require('../models/User');
const auth = require('../middleware/auth');
const upload = require('../multer').avatar;

const router = express.Router();


router.post('/', [upload.single('avatar')], async (req, res) => {
    const user = new User({
        username: req.body.username,
        password: req.body.password,
        displayName: req.body.displayName
    });
    if (req.file) {
        user.avatar = 'http://localhost:8000/' + req.file.filename;
    }
    try {
        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        setTimeout(() => res.status(400).send({error: "Username or password not correct!!!"}), 70);
        return;
    }

    const isMatch = await bcrypt.compare(req.body.password, user.password);

    if (!isMatch) {
        return res.status(400).send({error: "Username or password not correct!!!"});
    }
    try {
        user.generateToken();
        await user.save();
        return res.send(user);
    } catch (error) {
        return res.status(400).send(error);
    }
});

router.delete('/sessions', async (req, res) => {
    const success = {message: "Success"};
    try {
        const token = req.get('Authorization').split(" ")[1];
        if (!token) return res.send(success);
        const user = await User.findOne({token});
        if (!user) return res.send(success);
        user.generateToken();
        await user.save();
        return res.send(success);
    } catch (e) {
        return res.send(success);
    }
});
router.post('/facebook', async (req, res) => {
    try {
        const inputToken = req.body.accessToken;
        const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

        const url = `https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

        const response = await axios.get(url);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }

        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({message: 'User ID incorrect'});
        }

        let user = await User.findOne({facebookId: req.body.id});
        console.log(req.body);
        if (!user) {
            user = new User({
                username: req.body.id,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatar:req.body.picture.data.url
            });
        }

        user.generateToken();
        await user.save();

        return res.send(user);
    } catch (e) {
        return res.sendStatus(401);
    }
});

router.patch('/profile', [auth, upload.single('avatar')], async (req, res) => {
    try {
        if (req.body.password) {
            req.user.password = req.body.password;
        }

        if (req.file) {
            if (req.user.avatar) {
                const avatarPath = req.user.avatar.replace('http://localhost:8000/',"");
                await fs.promises.unlink(path.join(config.uploadPath, avatarPath));
            }

            req.user.avatar = 'http://localhost:8000/' + req.file.filename;
        }

        if (req.body.firstName) {
            req.user.firstName = req.body.firstName;
        }

        if (req.body.lastName) {
            req.user.lastName = req.body.lastName;
        }

        await req.user.save();

        return res.send(req.user);
    } catch (e) {
        console.error(e);
        return res.sendStatus(500);
    }
});
module.exports = router;