const mongoose = require("mongoose");
const nanoid = require('nanoid');
const config = require('./config');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const Track = require('./models/Track');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.database, config.databaseOptions);
    const collections = await mongoose.connection.db.listCollections().toArray();
    for (let coll of collections) {
        await mongoose.connection.db.dropCollection(coll.name);
    }
    const [admin, user] = await User.create({
        displayName:"User",
        username: 'user',
        password: '123',
        token: nanoid()
    }, {
        displayName:"Admin",
        username: 'admin',
        password: '123',
        role: 'admin',
        token: nanoid()
    });

    const [artist1, artist2, artist3] = await Artist.create({
            user: admin,
            name: 'zoloto',
            image: 'uploads/artistsImg/1.jpg',
            description: 'blabla',
            isPublished:true
        },
        {
            user: admin,
            name: 'dorn',
            image: 'uploads/artistsImg/1.jpg',
            description: 'blabla1'
        },
        {
            user: admin,
            name: 'zveri',
            image: 'uploads/artistsImg/1.jpg',
            description: 'blabla2'
        }
    );
    const [album1, album2, album3, album4, album5] = await Album.create({
            user: admin,
            artist: artist1,
            coverImage: 'uploads/albumsImg/1.jpg',
            title: 'blablatttt',
            releaseDate: '2018-02-03',
            isPublished:true
        },
        {
            user: user,
            artist: artist2,
            coverImage: 'uploads/albumsImg/1.jpg',
            title: 'blabla1tttewcew',
            releaseDate: '2016-02-03'
        },
        {
            user: user,
            artist: artist1,
            coverImage: 'uploads/albumsImg/1.jpg',
            title: 'blabla2cewcewcecewcewce',
            releaseDate: '2015-02-03'
        }, {
            user: user,
            artist: artist3,
            coverImage: 'uploads/albumsImg/1.jpg',
            title: 'qqqqqqqqqqqq',
            releaseDate: '2017-02-03'
        },
        {
            user: user,
            artist: artist3,
            coverImage: 'uploads/albumsImg/1.jpg',
            title: 'qwwwwwwwwwwwww',
            releaseDate: '2012-02-03'
        }
    );
    await Track.create({
            user: user,
            album: album1,
            trackNumber: '1',
            title: 'sun',
            duration: '3.1',
            youTubeLink: 'https://www.youtube.com/embed/u4OUSunprK0',
            isPublished:true
        },
        {
            user: user,
            album: album1,
            trackNumber: '2',
            title: 'snow',
            duration: '3.25',
            youTubeLink: 'https://www.youtube.com/embed/0ODsW1bdX94'
        },
        {
            user: user,
            album: album1,
            trackNumber: '3',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY',
            isPublished:true
        },
        {
            user: user,
            album: album1,
            trackNumber: '4',
            title: 'sun1',
            duration: '3.1',
            youTubeLink: 'https://www.youtube.com/embed/u4OUSunprK0',
            isPublished:true
        },
        {
            user: user,
            album: album1,
            trackNumber: '5',
            title: 'snow2',
            duration: '3.25',
            youTubeLink: ''
        },
        {
            user: user,
            album: album2,
            trackNumber: '1',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album2,
            trackNumber: '2',
            title: 'sun888',
            duration: '3.1',
            youTubeLink: 'https://www.youtube.com/embed/u4OUSunprK0'
        },
        {
            user: admin,
            album: album2,
            trackNumber: '3',
            title: 'snow888',
            duration: '3.25',
            youTubeLink: 'https://www.youtube.com/embed/0ODsW1bdX94'
        },
        {
            user: admin,
            album: album2,
            trackNumber: '4',
            title: 'you888',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album3,
            trackNumber: '1',
            title: 'sun',
            duration: '3.1',
            youTubeLink: 'https://www.youtube.com/embed/u4OUSunprK0'
        },
        {
            user: admin,
            album: album3,
            trackNumber: '2',
            title: 'snow',
            duration: '3.25',
            youTubeLink: 'https://www.youtube.com/embed/0ODsW1bdX94'
        },
        {
            user: admin,
            album: album3,
            trackNumber: '3',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album3,
            trackNumber: '4',
            title: 'sun',
            duration: '3.1',
            youTubeLink: 'https://www.youtube.com/embed/u4OUSunprK0'
        },
        {
            user: admin,
            album: album3,
            trackNumber: '5',
            title: 'snow',
            duration: '3.25',
            youTubeLink: 'https://www.youtube.com/embed/0ODsW1bdX94'
        },
        {
            user: admin,
            album: album4,
            trackNumber: '1',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album4,
            trackNumber: '2',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album4,
            trackNumber: '3',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album4,
            trackNumber: '4',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album4,
            trackNumber: '5',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album5,
            trackNumber: '1',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album5,
            trackNumber: '2',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album5,
            trackNumber: '3',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album5,
            trackNumber: '4',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
        {
            user: admin,
            album: album5,
            trackNumber: '5',
            title: 'you',
            duration: '2.59',
            youTubeLink: ''
        },
        {
            user: admin,
            album: album5,
            trackNumber: '6',
            title: 'you',
            duration: '2.59',
            youTubeLink: 'https://www.youtube.com/embed/G3iYV4m-QRY'
        },
    );
};
run().catch(e => {
    mongoose.connection.close();
    throw e;
});