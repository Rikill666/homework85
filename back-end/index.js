const express = require('express');
const cors = require('cors');
const albums= require('./app/albums');
const artists= require('./app/artists');
const tracks= require('./app/tracks');
const users= require('./app/users');
const trackHistories= require('./app/trackHistories');
const mongoose = require("mongoose");

const app = express();
app.use(cors());
const port = 8000;

app.use(express.json());
app.use(express.static('public'));


const run = async () =>{
    await mongoose.connect('mongodb://localhost/musicMaker', {
        useNewUrlParser:true,
        useUnifiedTopology:true,
        useCreateIndex:true
    });
    app.use('/albums', albums);
    app.use('/artists', artists);
    app.use('/tracks', tracks);
    app.use('/users', users);
    app.use('/track_history', trackHistories);
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
};
run().catch(e=>{
    console.log(e);
});


