const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const albumScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    title:{
        type: String,
        required: true
    },
    releaseDate:{
        type: Date,
        required: true
    },
    coverImage:{
        type: String
    },
    artist:{
        type: Scheme.Types.ObjectId,
        ref: 'Artist',
        required: true
    },
    isPublished:{
        type: Boolean,
        required: true,
        default: false,
    }
});

const Album = mongoose.model("Album", albumScheme);
module.exports = Album;