const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const artistScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    name:{
        type: String,
        required:true
    },
    image:{
        type: String
    },
    description:{
        type: String
    },
    isPublished:{
        type: Boolean,
        required: true,
        default: false,
    }
});

const Artist = mongoose.model("Artist", artistScheme);
module.exports = Artist;