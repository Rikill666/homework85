const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const trackScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    trackNumber:{
        type: Number,
        required:true
    },
    title:{
        type: String,
        required:true
    },
    duration:{
        type: Number,
        required:true
    },
    album:{
        type: Scheme.Types.ObjectId,
        ref: 'Album',
        required: true
    },
    youTubeLink:{
        type:String
    },
    isPublished:{
        type: Boolean,
        required: true,
        default: false,
    }
});

const Track = mongoose.model("Track", trackScheme);
module.exports = Track;