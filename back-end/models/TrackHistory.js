const mongoose = require("mongoose");

const Scheme = mongoose.Schema;

const trackHistoryScheme = new Scheme({
    user:{
        type: Scheme.Types.ObjectId,
        ref: 'User',
        required: true
    },
    track:{
        type: Scheme.Types.ObjectId,
        ref: 'Track',
        required: true
    },
    dateTime:{
        type: Date,
        required: true
    }
});

const TrackHistory = mongoose.model("TrackHistory", trackHistoryScheme);
module.exports = TrackHistory;