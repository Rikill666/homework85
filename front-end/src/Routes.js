import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import Artists from "./containers/Artists/Artists";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import ArtistForm from "./containers/ArtistForm/ArtistForm";
import AlbumForm from "./containers/AlbumForm/AlbumForm";
import TrackForm from "./containers/TrackForm/TrackForm";
import TrackHistory from "./containers/TrackHistory/TrackHistory";
import Albums from "./containers/Albums/Albums";
import Tracks from "./containers/Tracks/Tracks";
import {useSelector} from "react-redux";
import UserProfile from "./containers/UserProfile/UserProfile";

const ProtectedRoute = ({isAllowed, ...props})=>(
    isAllowed ? <Route {...props}/>:<Redirect to="/login"/>
);

const Routes = () => {
    const user = useSelector(state=>state.users.user);
    return (
        <Switch>
            <Route path="/" exact component={Artists}/>
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <ProtectedRoute isAllowed={user} path="/profile" exact component={UserProfile} />
            <ProtectedRoute isAllowed={user} path="/add_artist" exact component={ArtistForm}/>
            <ProtectedRoute isAllowed={user} path="/add_album" exact component={AlbumForm}/>
            <ProtectedRoute isAllowed={user} path="/add_track" exact component={TrackForm}/>
            <ProtectedRoute isAllowed={user} path="/track_history" exact component={TrackHistory}/>
            <Route path="/:id/albums" exact component={Albums}/>
            <Route path="/:id/albums/:id/tracks" exact component={Tracks}/>
            <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
    );
};

export default Routes;