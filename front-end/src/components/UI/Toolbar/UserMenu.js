import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, NavItem, NavLink, UncontrolledDropdown} from "reactstrap";
import {Link, NavLink as RouterNavLink} from "react-router-dom";

const UserMenu = ({user, logout}) => {
    return (
        <>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/add_artist" exact>Create new artist</NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/add_album" exact>Create new album</NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/add_track" exact>Create new track</NavLink>
            </NavItem>
            <NavItem>
                <NavLink tag={RouterNavLink} to="/track_history" exact>Track history</NavLink>
            </NavItem>
            <NavItem>
                {user.avatar ? <img style={{width:"50px"}} src={user.avatar} alt=""/> : null}
            </NavItem>
            <UncontrolledDropdown nav inNavbar>
                <DropdownToggle nav caret>
                    Hello, {user.displayName}!
                </DropdownToggle>
                <DropdownMenu right>
                    <DropdownItem tag={Link} to="/profile">
                        Profile
                    </DropdownItem>
                    <DropdownItem tag={RouterNavLink} to="/track_history" exact>
                        Track history
                    </DropdownItem>
                    <DropdownItem divider/>
                    <DropdownItem onClick={logout}>
                        Logout
                    </DropdownItem>
                </DropdownMenu>
            </UncontrolledDropdown>
        </>
    );
};

export default UserMenu;