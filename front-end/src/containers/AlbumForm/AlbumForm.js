import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {getArtists} from "../../store/actions/artistsActions";
import {createAlbum} from "../../store/actions/albumsActions";

class AlbumForm extends Component {
    componentDidMount() {
        this.props.getArtists();
    };
    state = {
        title: "",
        releaseDate: "",
        coverImage: "",
        artist: ""
    };
    inputChangeHandler = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };
    submitFormHandler = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        await this.props.createAlbum(formData);
        this.props.history.push("/"+this.state.artist+"/albums")
    };
    getFieldError = fieldName => {
        try {
            if (fieldName === "artist" && this.props.error.errors[fieldName].message) {
                return "Path `artist` is required."
            }
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    render() {
        const artistsOptions = this.props.artists.map(artist => ({title: artist.name, id: artist._id}));
        return (
            <>
                <h2>New album: </h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        title="Artist"
                        propertyName="artist"
                        error={this.getFieldError('artist')}
                        type="select"
                        options={artistsOptions}
                        value={this.state.artist}
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        type="text"
                        propertyName="title"
                        title="Title"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('title')}
                        placeholder='Enter title'
                    />
                    <FormElement
                        type="date"
                        propertyName="releaseDate"
                        title="Release date"
                        value={this.state.releaseDate}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('releaseDate')}
                        placeholder='Enter release date'
                    />
                    <FormElement
                        type="file"
                        propertyName="coverImage"
                        title="Cover image"
                        onChange={this.fileChangeHandler}
                        error={this.getFieldError('coverImage')}
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Create
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        error: state.albums.createError,
        artists: state.artists.artists
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createAlbum: (albumData) => dispatch(createAlbum(albumData)),
        getArtists: () => dispatch(getArtists()),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(AlbumForm);