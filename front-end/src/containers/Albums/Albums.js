import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteAlbum, getAlbums, publishAlbum} from "../../store/actions/albumsActions";
import Spinner from "../../components/UI/Spinner/Spinner";
import {Button, Card, CardBody, CardImg, CardSubtitle, CardTitle, Col, Row} from "reactstrap";
import {NavLink} from "react-router-dom";
import {apiURL} from "../../constants";
import Moment from "react-moment";
import {getArtistById} from "../../store/actions/artistsActions";
import ShowTo from "../../hoc/ShowTo";

class Albums extends Component {
    componentDidMount() {
        this.props.getAlbums(this.props.match.params.id);
        this.props.getArtistById(this.props.match.params.id);
    }
    delete = async (id) =>{
       await this.props.deleteAlbum(id);
       await this.props.getAlbums(this.props.match.params.id);
    };
    render() {
        return (this.props.loading ? <Spinner/> :
                this.props.albums.length > 0 && this.props.artist ? <>
                    <h2>Artist: {this.props.artist.name}</h2>
                    <Row style={{color: "black", textTransform: "none"}}>
                        {this.props.albums.map(album => {
                            if (album.isPublished) {
                                let link = this.props.user ? this.props.history.location.pathname + "/" + album._id + "/tracks" : "/login";
                                return <Col sm={12} md={6} lg={4} key={album._id} style={{height: '100%'}}>
                                    <ShowTo role={"admin"}>
                                        <Button onClick={()=>this.delete(album._id)}>Delete</Button>
                                    </ShowTo>
                                    <NavLink style={{color: "black", textDecoration: "none"}}
                                             to={link} exact>
                                        <Card style={{marginBottom: "10px", textAlign: "center"}}>
                                            <CardBody>
                                                {album.coverImage ?
                                                    <CardImg top width="100%" alt="img"
                                                             src={apiURL + '/' + album.coverImage}
                                                    />
                                                    : null}
                                                <CardTitle style={{
                                                    fontSize: "25px",
                                                    fontWeight: "bold",
                                                    margin: "10px 0 0"
                                                }}>
                                                    {album.title}
                                                </CardTitle>
                                                <CardSubtitle>
                                                    <Moment format="YYYY-MM-DD">
                                                        {album.releaseDate}
                                                    </Moment>
                                                </CardSubtitle>
                                                <CardSubtitle>
                                                    Songs count: {album.count}
                                                </CardSubtitle>
                                            </CardBody>
                                        </Card>
                                    </NavLink>
                                </Col>
                            }else {
                                let link = this.props.user ? this.props.history.location.pathname + "/" + album._id + "/tracks" : "/login";
                                return <ShowTo role={"admin"} key={album._id}>
                                    <Col sm={12} md={6} lg={4} key={album._id} style={{height: '100%'}}>
                                        <Row>
                                            <Col sm={6} md={6} lg={6}>
                                                <h3>No public</h3>
                                            </Col>
                                            <Col sm={3} md={3} lg={3}>
                                                <Button onClick={()=>this.props.publishAlbum(album._id)}>Public</Button>
                                            </Col>
                                            <Col sm={3} md={3} lg={3}>
                                                <Button onClick={()=>this.delete(album._id)}>Delete</Button>
                                            </Col>
                                        </Row>
                                        <NavLink style={{color: "black", textDecoration: "none"}}
                                                 to={link} exact>
                                            <Card style={{marginBottom: "10px", textAlign: "center"}}>
                                                <CardBody>
                                                    {album.coverImage ?
                                                        <CardImg top width="100%" alt="img"
                                                                 src={apiURL + '/' + album.coverImage}
                                                        />
                                                        : null}
                                                    <CardTitle style={{
                                                        fontSize: "25px",
                                                        fontWeight: "bold",
                                                        margin: "10px 0 0"
                                                    }}>
                                                        {album.title}
                                                    </CardTitle>
                                                    <CardSubtitle>
                                                        <Moment format="YYYY-MM-DD">
                                                            {album.releaseDate}
                                                        </Moment>
                                                    </CardSubtitle>
                                                    <CardSubtitle>
                                                        Songs count: {album.count}
                                                    </CardSubtitle>
                                                </CardBody>
                                            </Card>
                                        </NavLink>
                                    </Col>
                                </ShowTo>
                            }})}
                    </Row>
                </> : <h3>No albums</h3>
        )
    }
}


const mapStateToProps = state => {
    return {
        albums: state.albums.albums,
        loading: state.albums.loading,
        error: state.albums.error,
        artist: state.artists.artist,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getAlbums: (artistId) => dispatch(getAlbums(artistId)),
        getArtistById: (artistId) => dispatch(getArtistById(artistId)),
        deleteAlbum:(id) => dispatch(deleteAlbum(id)),
        publishAlbum:(id) => dispatch(publishAlbum(id)),

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Albums);