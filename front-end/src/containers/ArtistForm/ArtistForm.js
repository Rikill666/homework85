import React, {Component} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {createArtist} from "../../store/actions/artistsActions";

class ArtistForm extends Component {
    state = {
        name: "",
        description: "",
        image: "",
    };
    inputChangeHandler = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };
    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };
    submitFormHandler = async event => {
        event.preventDefault();
        const formData = new FormData();
        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });
        await this.props.createArtist(formData);
    };
    getFieldError = fieldName => {
        try {
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    render() {
        return (
            <>
                <h2>New post: </h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        type="text"
                        propertyName="name"
                        title="Name"
                        value={this.state.name}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('name')}
                        placeholder='Enter name'
                    />
                    <FormElement
                        type="text"
                        propertyName="description"
                        title="Description"
                        value={this.state.description}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('description')}
                        placeholder='Enter description'
                    />
                    <FormElement
                        type="file"
                        propertyName="image"
                        title="Image"
                        onChange={this.fileChangeHandler}
                        error={this.getFieldError('image')}
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Create
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        error: state.artists.createError,
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createArtist: (artistData) => dispatch(createArtist(artistData)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ArtistForm);