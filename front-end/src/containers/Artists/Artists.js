import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteArtist, getArtists, publishArtist} from "../../store/actions/artistsActions";
import {Button, Card, CardBody, CardImg, CardTitle, Col, Row} from "reactstrap";
import Spinner from "../../components/UI/Spinner/Spinner";
import {NavLink} from "react-router-dom";
import {apiURL} from "../../constants";
import ShowTo from "../../hoc/ShowTo";

class Artists extends Component {
    componentDidMount() {
        this.props.getArtists();
    }
    delete = async (id) =>{
        await this.props.deleteArtist(id);
        await this.props.getArtists();
    };
    render() {
        return (this.props.loading ? <Spinner/> :
                <Row>
                    {this.props.artists.length > 0 ? this.props.artists.map(artist => {
                        if (artist.isPublished) {
                            return <Col sm={12} md={6} lg={4} key={artist._id}>
                                <ShowTo role={"admin"}>
                                    <Button onClick={()=>this.delete(artist._id)}>Delete</Button>
                                </ShowTo>
                                <NavLink style={{color: "black", textDecoration: "none"}} to={artist._id + "/albums"}
                                         exact>
                                    <Card style={{marginBottom: "10px"}}>
                                        <CardBody>

                                            {artist.image ?
                                                <CardImg top width="100%" alt="img"
                                                         src={apiURL + '/' + artist.image}/>
                                                : null}
                                            <CardTitle style={{
                                                textAlign: "center",
                                                fontSize: "25px",
                                                fontWeight: "bold",
                                                margin: "10px 0 0"
                                            }}>
                                                {artist.name}
                                            </CardTitle>
                                        </CardBody>
                                    </Card>
                                </NavLink>
                            </Col>
                        } else {
                            return <ShowTo role={"admin"} key={artist._id}>
                                <Col sm={12} md={6} lg={4}>
                                    <Row>
                                        <Col sm={6} md={6} lg={6}>
                                            <h3>No public</h3>
                                        </Col>
                                        <Col sm={3} md={3} lg={3}>
                                            <Button onClick={()=>this.props.publishArtist(artist._id)}>Public</Button>
                                        </Col>
                                        <Col sm={3} md={3} lg={3}>
                                            <Button onClick={()=>this.delete(artist._id)}>Delete</Button>
                                        </Col>
                                    </Row>
                                    <NavLink style={{color: "black", textDecoration: "none"}}
                                             to={artist._id + "/albums"}
                                             exact>
                                        <Card style={{marginBottom: "10px"}}>
                                            <CardBody>

                                                {artist.image ?
                                                    <CardImg top width="100%" alt="img"
                                                             src={apiURL + '/' + artist.image}/>
                                                    : null}
                                                <CardTitle style={{
                                                    textAlign: "center",
                                                    fontSize: "25px",
                                                    fontWeight: "bold",
                                                    margin: "10px 0 0"
                                                }}>
                                                    {artist.name}
                                                </CardTitle>
                                            </CardBody>
                                        </Card>
                                    </NavLink>
                                </Col>
                            </ShowTo>
                        }
                    }) : <h4>No artists</h4>}
                </Row>
        );
    }
}

const mapStateToProps = state => ({
    artists: state.artists.artists,
    loading: state.artists.loading,
    error: state.artists.error,
    user: state.users.user
});

const mapDispatchToProps = dispatch => ({
    getArtists: () => dispatch(getArtists()),
    deleteArtist:(id)=> dispatch(deleteArtist(id)),
    publishArtist:(id)=> dispatch(publishArtist(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Artists);