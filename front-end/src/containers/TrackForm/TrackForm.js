import React, {Component} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {getAlbums} from "../../store/actions/albumsActions";
import {getArtists} from "../../store/actions/artistsActions";
import {connect} from "react-redux";
import {createTrack} from "../../store/actions/tracksActions";

class TrackForm extends Component {
    async componentDidMount() {
        await this.props.getArtists();
        await this.props.getAlbums();
    };

    state = {
        artist:"",
        title: "",
        duration: "",
        trackNumber: "",
        album: "",
        youTubeLink: ""
    };
    inputChangeHandler = (event) => {
        if(event.target.name === "artist"){
            this.props.getAlbums(event.target.value);
        }
        this.setState({[event.target.name]: event.target.value});
    };

    submitFormHandler = async event => {
        event.preventDefault();
        await this.props.createTrack({...this.state});
    };
    getFieldError = fieldName => {
        try {
            if (fieldName === "artist" && this.props.error.errors[fieldName].message) {
                return "Path `artist` is required."
            }
            return this.props.error.errors[fieldName].message;
        } catch (e) {
            return undefined;
        }
    };

    render() {
        const artistsOptions = this.props.artists.map(artist => ({title: artist.name, id: artist._id}));
        const albumsOptions = this.props.albums.map(album => ({title: album.title, id: album._id}));
        return (
            <>
                <h2>New track: </h2>
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        title="Artist"
                        propertyName="artist"
                        error={this.getFieldError('artist')}
                        type="select"
                        options={artistsOptions}
                        value={this.state.artist}
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        title="Album"
                        propertyName="album"
                        error={this.getFieldError('album')}
                        type="select"
                        options={albumsOptions}
                        value={this.state.album}
                        onChange={this.inputChangeHandler}
                    />
                    <FormElement
                        type="text"
                        propertyName="title"
                        title="Title"
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('title')}
                        placeholder='Enter title'
                    />
                    <FormElement
                        type="number"
                        propertyName="trackNumber"
                        title="Track number"
                        value={this.state.trackNumber}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('trackNumber')}
                        placeholder='Enter track number'
                    />
                    <FormElement
                        type="number"
                        propertyName="duration"
                        title="Duration"
                        value={this.state.duration}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('duration')}
                        placeholder='Enter duration'
                    />
                    <FormElement
                        type="text"
                        propertyName="youTubeLink"
                        title="YouTubeLink"
                        value={this.state.youTubeLink}
                        onChange={this.inputChangeHandler}
                        error={this.getFieldError('youTubeLink')}
                        placeholder='Enter YouTubeLink'
                    />
                    <FormGroup row>
                        <Col sm={{offset: 2, size: 10}}>
                            <Button type="submit" color="primary">
                                Create
                            </Button>
                        </Col>
                    </FormGroup>
                </Form>
            </>
        );
    }
}

const mapStateToProps = state => {
    return {
        user: state.users.user,
        error: state.albums.createError,
        artists: state.artists.artists,
        albums: state.albums.albums
    };
};

const mapDispatchToProps = dispatch => {
    return {
        createTrack: (trackData) => dispatch(createTrack(trackData)),
        getArtists: () => dispatch(getArtists()),
        getAlbums: (artistId) => dispatch(getAlbums(artistId)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);