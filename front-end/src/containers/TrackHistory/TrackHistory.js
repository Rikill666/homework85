import React, {Component} from 'react';
import {connect} from "react-redux";
import {getTracksHistory} from "../../store/actions/trackHistoriesActions";
import Spinner from "../../components/UI/Spinner/Spinner";
import {Table} from "reactstrap";
import Moment from "react-moment";

class TrackHistory extends Component {
    componentDidMount() {
        this.props.getTracksHistory();
    }

    render() {
         return (this.props.loading ? <Spinner/> :
                this.props.tracks.length > 0 ? <>
                    <h4 style={{margin:'20px 0'}}>You listened</h4>
                    <Table>
                        <thead>
                        <tr>
                            <th>Artist</th>
                            <th>Album</th>
                            <th>Song title</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.tracks.map(track => {
                            return <tr key={track.id}>
                                <td>{track.artistName}</td>
                                <td>{track.albumTitle}</td>
                                <td>{track.trackTitle}</td>
                                <td>
                                    <Moment format="YYYY-MM-DD HH:mm:ss">
                                        {track.dateTime}
                                    </Moment>
                                </td>
                            </tr>
                        })}
                        </tbody>
                    </Table>
                </> : <h3>No tracks</h3>
        );
    }
}
const mapStateToProps = state => ({
    tracks: state.tracksHistory.tracks,
    loading: state.tracksHistory.historyLoading,
    error: state.tracksHistory.historyError
});

const mapDispatchToProps = dispatch => ({
    getTracksHistory: () => dispatch(getTracksHistory()),
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);