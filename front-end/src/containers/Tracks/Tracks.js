import React, {Component} from 'react';
import {Button, Table} from "reactstrap";
import {getAlbumById} from "../../store/actions/albumsActions";
import {connect} from "react-redux";
import {deleteTrack, getTracks, publishTrack} from "../../store/actions/tracksActions";
import Spinner from "../../components/UI/Spinner/Spinner";
import './Tracks.css';
import {createTrackHistory} from "../../store/actions/trackHistoriesActions";
import Modal from "../../components/UI/Modal/Modal";
import ShowTo from "../../hoc/ShowTo";

class Tracks extends Component {
    state = {
        modalShow: false,
        trackPlay: ""
    };
    delete = async (id) =>{
        await this.props.deleteTrack(id);
        this.props.getTracks(this.props.match.params.id)
    };

    componentDidMount() {
        if (this.props.user) {
            this.props.getTracks(this.props.match.params.id);
            this.props.getAlbumById(this.props.match.params.id);
        } else {
            this.props.history.push('/login');
        }
    }

    listenTrack = (track) => {
        this.props.createTrackHistory(track._id);
        if (track.youTubeLink) {
            this.setState({modalShow: true, trackPlay: track});
        }

    };
    modalHide = () => {
        this.setState({modalShow: false});
    };

    render() {

        return (this.props.loading ? <Spinner/> :
                this.props.tracks.length > 0 && this.props.album ? <>
                    <h2>Artist: {this.props.album.artist.name}</h2>
                    <h4 style={{margin: '20px 0'}}>Album: {this.props.album.title}</h4>
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Title</th>
                            <th>Duration</th>
                            <th/>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>
                        {this.props.tracks.map(track => {
                            if (track.isPublished) {
                                return <tr key={track._id}>
                                    <td style={{cursor: 'pointer'}} onClick={() => this.listenTrack(track)}>{track.trackNumber}</td>
                                    <td style={{cursor: 'pointer'}} onClick={() => this.listenTrack(track)}>{track.title}</td>
                                    <td style={{cursor: 'pointer'}} onClick={() => this.listenTrack(track)}>{track.duration} min</td>
                                    <td style={{cursor: 'pointer'}} onClick={() => this.listenTrack(track)}/>
                                    <td>
                                        <ShowTo role={"admin"}>
                                            <Button onClick={() => this.delete(track._id)}>Delete</Button>
                                        </ShowTo>
                                    </td>
                                </tr>
                            } else return <ShowTo role={"admin"} key={track._id}>
                                <tr key={track._id}>
                                    <td style={{cursor: 'pointer'}}
                                        onClick={() => this.listenTrack(track)}>{track.trackNumber}</td>
                                    <td style={{cursor: 'pointer'}}
                                        onClick={() => this.listenTrack(track)}>{track.title}</td>
                                    <td style={{cursor: 'pointer'}}
                                        onClick={() => this.listenTrack(track)}>{track.duration} min
                                    </td>
                                    <td><Button onClick={() => this.props.publishTrack(track._id)}>Public</Button></td>
                                    <td><Button onClick={() => this.delete(track._id)}>Delete</Button></td>
                                </tr>
                            </ShowTo>
                        })}
                        </tbody>
                    </Table>
                    <Modal show={this.state.modalShow} close={this.modalHide}>
                        <iframe
                            title={this.state.trackPlay._id}
                            type="text/html" width="300" height="200"
                            src={this.state.trackPlay.youTubeLink + "?autoplay=1&origin=http://example.com"}
                            frameBorder="0"/>
                    </Modal>
                </> : <h3>No tracks</h3>
        );
    }
}

const mapStateToProps = state => {
    return {
        tracks: state.tracks.tracks,
        loading: state.tracks.loading,
        error: state.tracks.error,
        album: state.albums.album,
        user: state.users.user
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getTracks: (albumId) => dispatch(getTracks(albumId)),
        getAlbumById: (albumId) => dispatch(getAlbumById(albumId)),
        createTrackHistory: (trackId) => dispatch(createTrackHistory(trackId)),
        deleteTrack: (id) => dispatch(deleteTrack(id)),
        publishTrack: (id) => dispatch(publishTrack(id)),
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Tracks);