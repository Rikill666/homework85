import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Button, Col, Form, FormGroup, Row} from "reactstrap";
import FormElement from "../../components/UI/FormElement/FormElement";
import {editProfile} from "../../store/actions/usersActions";
import {apiURL} from "../../constants";


const UserProfile = () => {
  const dispatch = useDispatch();
  const user = useSelector(state => state.users.user);

  const [state, setState] = useState({
    password: '',
    avatar: user.avatar || null,
    displayName: user.displayName || '',
  });

  const onSubmit = e => {
    e.preventDefault();

    const profileData = new FormData();

    Object.keys(state).forEach(key => {
      profileData.append(key, state[key]);
    });

    dispatch(editProfile(profileData));
  };

  const onChange = e => {
    setState({...state, [e.target.name]: e.target.value});
  };

  const onFileChange = e => {
    setState({...state, [e.target.name]: e.target.files[0]});
  };

  return (
    <>
      <h3>Change user profile</h3>
      <Form onSubmit={onSubmit}>
        <FormElement
          type="password"
          propertyName="password"
          title="Change password"
          onChange={onChange}
          value={state.password}
        />
        <FormElement
          type="file"
          propertyName="avatar"
          title="Upload avatar"
          onChange={onFileChange}
        />
        <Row>
          <Col sm={2}>
            Currently selected avatar:
          </Col>
          <Col sm={10}>
            {user.avatar ? (
              <img src={user.avatar} alt={user.username} style={{maxWidth: '100px'}}/>
            ) : 'No avatar selected'}
          </Col>
        </Row>
        <FormElement
          propertyName="displayName"
          title="Display name"
          onChange={onChange}
          value={state.displayName}
        />
        <FormGroup row>
          <Col sm={{offset: 2, size: 10}}>
            <Button type="submit" color="primary">
              Edit profile
            </Button>
          </Col>
        </FormGroup>
      </Form>
    </>
  );
};

export default UserProfile;