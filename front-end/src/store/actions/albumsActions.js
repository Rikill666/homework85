import axiosApi from "../../axiosApi";
import {createArtistError} from "./artistsActions";

export const FETCH_ALBUMS_REQUEST = 'FETCH_ALBUMS_REQUEST';
export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const FETCH_ALBUMS_ERROR = 'FETCH_ALBUMS_ERROR';

export const FETCH_ALBUM_BY_ID_REQUEST = 'FETCH_ALBUM_BY_ID_REQUEST';
export const FETCH_ALBUM_BY_ID_SUCCESS = 'FETCH_ALBUM_BY_ID_SUCCESS';
export const FETCH_ALBUM_BY_ID_ERROR = 'FETCH_ALBUM_BY_ID_ERROR';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_ERROR = 'CREATE_ALBUM_ERROR';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_ERROR = 'DELETE_ALBUM_ERROR';

export const PUBLISH_ALBUM_REQUEST = 'PUBLISH_ALBUM_REQUEST';
export const PUBLISH_ALBUM_SUCCESS = 'PUBLISH_ALBUM_SUCCESS';
export const PUBLISH_ALBUM_ERROR = 'PUBLISH_ALBUM_ERROR';

export const deleteAlbumRequest = () => {return {type: DELETE_ALBUM_REQUEST};};
export const deleteAlbumSuccess = () => ({type: DELETE_ALBUM_SUCCESS});
export const deleteAlbumError = (error) => {return {type: DELETE_ALBUM_ERROR, error};};

export const publishAlbumRequest = () => {return {type: PUBLISH_ALBUM_REQUEST};};
export const publishAlbumSuccess = (album) => ({type: PUBLISH_ALBUM_SUCCESS, album});
export const publishAlbumError = (error) => {return {type: PUBLISH_ALBUM_ERROR, error};};

export const createAlbumRequest = () => {return {type: CREATE_ALBUM_REQUEST};};
export const createAlbumSuccess = () => ({type: CREATE_ALBUM_SUCCESS});
export const createAlbumError = (error) => {return {type: CREATE_ALBUM_ERROR, error};};

export const fetchAlbumByIdRequest = () => {return {type: FETCH_ALBUM_BY_ID_REQUEST};};
export const fetchAlbumByIdSuccess = album => ({type: FETCH_ALBUM_BY_ID_SUCCESS, album});
export const fetchAlbumByIdError = (error) => {return {type: FETCH_ALBUM_BY_ID_ERROR, error};};

export const fetchAlbumsRequest = () => {return {type: FETCH_ALBUMS_REQUEST};};
export const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
export const fetchAlbumsError = (error) => {return {type: FETCH_ALBUMS_ERROR, error};};

export const deleteAlbum = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteAlbumRequest());
            await axiosApi.delete('/albums/' + id);
            dispatch(deleteAlbumSuccess());
        } catch (e) {
            dispatch(deleteAlbumError(e));
        }
    };
};
export const publishAlbum = (id) => {
    return async dispatch => {
        try {
            dispatch(publishAlbumRequest());
            const album = await axiosApi.post('/albums/publish/' + id);
            dispatch(publishAlbumSuccess(album.data));
        } catch (e) {
            dispatch(publishAlbumError(e));
        }
    };
};

export const createAlbum = (albumData) => {
    return async dispatch => {
        try {
            dispatch(createAlbumRequest());
            await axiosApi.post('/albums', albumData);
            dispatch(createAlbumSuccess());
        } catch (error) {
            if (error.response) {
                dispatch(createAlbumError(error.response.data));
            } else {
                dispatch(createArtistError({global: "Network error or no internet"}));
            }
        }
    };
};

export const getAlbumById = (albumId) => {
    return async dispatch => {
        try{
            dispatch(fetchAlbumByIdRequest());
            const response = await axiosApi.get('/albums/'+ albumId);
            const album = response.data;
            dispatch(fetchAlbumByIdSuccess(album));
        }
        catch (e) {
            dispatch(fetchAlbumByIdError(e));
        }
    };
};

export const getAlbums = (artistId) => {
    let url = '/albums';
    if(artistId)
    {
        url+=('?artist='+ artistId);
    }
    return async dispatch => {
        try{
            dispatch(fetchAlbumsRequest());
            const response = await axiosApi.get(url);
            const albums = response.data;
            dispatch(fetchAlbumsSuccess(albums));
        }
        catch (e) {
            dispatch(fetchAlbumsError(e));
        }
    };
};