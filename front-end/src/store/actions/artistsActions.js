import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_ARTISTS_REQUEST = 'FETCH_ARTISTS_REQUEST';
export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const FETCH_ARTISTS_ERROR = 'FETCH_ARTISTS_ERROR';

export const FETCH_ARTIST_BY_ID_REQUEST = 'FETCH_ARTIST_BY_ID_REQUEST';
export const FETCH_ARTIST_BY_ID_SUCCESS = 'FETCH_ARTIST_BY_ID_SUCCESS';
export const FETCH_ARTIST_BY_ID_ERROR = 'FETCH_ARTIST_BY_ID_ERROR';

export const CREATE_ARTIST_REQUEST = 'CREATE_ARTIST_REQUEST';
export const CREATE_ARTIST_SUCCESS = 'CREATE_ARTIST_SUCCESS';
export const CREATE_ARTIST_ERROR = 'CREATE_ARTIST_ERROR';

export const DELETE_ARTIST_REQUEST = 'DELETE_ARTIST_REQUEST';
export const DELETE_ARTIST_SUCCESS = 'DELETE_ARTIST_SUCCESS';
export const DELETE_ARTIST_ERROR = 'DELETE_ARTIST_ERROR';

export const PUBLISH_ARTIST_REQUEST = 'PUBLISH_ARTIST_REQUEST';
export const PUBLISH_ARTIST_SUCCESS = 'PUBLISH_ARTIST_SUCCESS';
export const PUBLISH_ARTIST_ERROR = 'PUBLISH_ARTIST_ERROR';

export const deleteArtistRequest = () => {return {type: DELETE_ARTIST_REQUEST};};
export const deleteArtistSuccess = () => ({type: DELETE_ARTIST_SUCCESS});
export const deleteArtistError = (error) => {return {type: DELETE_ARTIST_ERROR, error};};

export const publishArtistRequest = () => {return {type: PUBLISH_ARTIST_REQUEST};};
export const publishArtistSuccess = (artist) => ({type: PUBLISH_ARTIST_SUCCESS, artist});
export const publishArtistError = (error) => {return {type: PUBLISH_ARTIST_ERROR, error};};

export const createArtistRequest = () => {return {type: CREATE_ARTIST_REQUEST};};
export const createArtistSuccess = () => ({type: CREATE_ARTIST_SUCCESS});
export const createArtistError = (error) => {return {type: CREATE_ARTIST_ERROR, error};};

export const fetchArtistByIdRequest = () => {return {type: FETCH_ARTIST_BY_ID_REQUEST};};
export const fetchArtistByIdSuccess = artist => ({type: FETCH_ARTIST_BY_ID_SUCCESS, artist});
export const fetchArtistByIdError = (error) => {return {type: FETCH_ARTIST_BY_ID_ERROR, error};};

export const fetchArtistsRequest = () => {return {type: FETCH_ARTISTS_REQUEST};};
export const fetchArtistsSuccess = artists => ({type: FETCH_ARTISTS_SUCCESS, artists});
export const fetchArtistsError = (error) => {return {type: FETCH_ARTISTS_ERROR, error};};

export const deleteArtist = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteArtistRequest());
            await axiosApi.delete('/artists/' + id);
            dispatch(deleteArtistSuccess());
            dispatch(getArtists());
        } catch (e) {
            dispatch(deleteArtistError(e));
        }
    };
};
export const publishArtist = (id) => {
    return async dispatch => {
        try {
            dispatch(publishArtistRequest());
            const artist = await axiosApi.post('/artists/publish/' + id);
            dispatch(publishArtistSuccess(artist.data));
        } catch (e) {
            dispatch(publishArtistError(e));
        }
    };
};

export const createArtist = (artistData) => {
    return async dispatch => {
        try {
            dispatch(createArtistRequest());
            await axiosApi.post('/artists', artistData);
            dispatch(createArtistSuccess());
            dispatch(push('/'));
        } catch (error) {
            if (error.response) {
                dispatch(createArtistError(error.response.data));
            } else {
                dispatch(createArtistError({global: "Network error or no internet"}));
            }
        }
    };
};

export const getArtistById = (artistId) => {
    return async dispatch => {
        try{
            dispatch(fetchArtistByIdRequest());
            const response = await axiosApi.get('/artists/' + artistId);
            const artist = response.data;
            dispatch(fetchArtistByIdSuccess(artist));
        }
        catch (e) {
            dispatch(fetchArtistByIdError(e));
        }
    };
};

export const getArtists = () => {
    return async dispatch => {
        try{
            dispatch(fetchArtistsRequest());
            const response = await axiosApi.get('/artists');
            const artists = response.data;
            dispatch(fetchArtistsSuccess(artists));
        }
        catch (e) {
            dispatch(fetchArtistsError(e));
        }
    };
};