import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_TRACKS_HISTORY_REQUEST = 'FETCH_TRACKS_HISTORY_REQUEST';
export const FETCH_TRACKS_HISTORY_SUCCESS = 'FETCH_TRACKS_HISTORY_SUCCESS';
export const FETCH_TRACKS_HISTORY_ERROR = 'FETCH_TRACKS_HISTORY_ERROR';

export const CREATE_TRACK_HISTORY_REQUEST = 'CREATE_TRACK_HISTORY_REQUEST';
export const CREATE_TRACK_HISTORY_SUCCESS = 'CREATE_TRACK_HISTORY_SUCCESS';
export const CREATE_TRACK_HISTORY_ERROR = 'CREATE_TRACK_HISTORY_ERROR';

export const createTrackHistoryRequest = () => {return {type: CREATE_TRACK_HISTORY_REQUEST};};
export const createTrackHistorySuccess = () => ({type: CREATE_TRACK_HISTORY_SUCCESS});
export const createTrackHistoryError = (error) => {return {type: CREATE_TRACK_HISTORY_ERROR, error};};

export const fetchTracksHistoryRequest = () => {return {type: FETCH_TRACKS_HISTORY_REQUEST};};
export const fetchTracksHistorySuccess = tracks => ({type: FETCH_TRACKS_HISTORY_SUCCESS, tracks});
export const fetchTracksHistoryError = (error) => {return {type: FETCH_TRACKS_HISTORY_ERROR, error};};

export const createTrackHistory = (trackId) => {
    return async dispatch => {
        try{
            dispatch(createTrackHistoryRequest());
            await axiosApi.post('/track_history?track='+ trackId);
            dispatch(createTrackHistorySuccess());
        }
        catch (e) {
            dispatch(createTrackHistoryError(e));
        }
    };
};

export const getTracksHistory = () => {
    return async dispatch => {
        try{
            dispatch(fetchTracksHistoryRequest());
            const response = await axiosApi.get('/track_history');
            const tracks = response.data;
            dispatch(fetchTracksHistorySuccess(tracks));
        }
        catch (e) {
            dispatch(fetchTracksHistoryError(e));
            dispatch(push('/login'));
        }
    };
};