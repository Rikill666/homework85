import axiosApi from "../../axiosApi";
import {push} from "connected-react-router";

export const FETCH_TRACKS_REQUEST = 'FETCH_TRACKS_REQUEST';
export const FETCH_TRACKS_SUCCESS = 'FETCH_TRACKS_SUCCESS';
export const FETCH_TRACKS_ERROR = 'FETCH_TRACKS_ERROR';

export const CREATE_TRACK_REQUEST = 'CREATE_TRACK_REQUEST';
export const CREATE_TRACK_SUCCESS = 'CREATE_TRACK_SUCCESS';
export const CREATE_TRACK_ERROR = 'CREATE_TRACK_ERROR';

export const DELETE_TRACK_REQUEST = 'DELETE_TRACK_REQUEST';
export const DELETE_TRACK_SUCCESS = 'DELETE_TRACK_SUCCESS';
export const DELETE_TRACK_ERROR = 'DELETE_TRACK_ERROR';

export const PUBLISH_TRACK_REQUEST = 'PUBLISH_TRACK_REQUEST';
export const PUBLISH_TRACK_SUCCESS = 'PUBLISH_TRACK_SUCCESS';
export const PUBLISH_TRACK_ERROR = 'PUBLISH_TRACK_ERROR';

export const deleteTrackRequest = () => {return {type: DELETE_TRACK_REQUEST};};
export const deleteTrackSuccess = () => ({type: DELETE_TRACK_SUCCESS});
export const deleteTrackError = (error) => {return {type: DELETE_TRACK_ERROR, error};};

export const publishTrackRequest = () => {return {type: PUBLISH_TRACK_REQUEST};};
export const publishTrackSuccess = (track) => ({type: PUBLISH_TRACK_SUCCESS, track});
export const publishTrackError = (error) => {return {type: PUBLISH_TRACK_ERROR, error};};

export const createTrackRequest = () => {return {type: CREATE_TRACK_REQUEST};};
export const createTrackSuccess = () => ({type: CREATE_TRACK_SUCCESS});
export const createTrackError = (error) => {return {type: CREATE_TRACK_ERROR, error};};

export const fetchTracksRequest = () => {return {type: FETCH_TRACKS_REQUEST};};
export const fetchTracksSuccess = tracks => ({type: FETCH_TRACKS_SUCCESS, tracks});
export const fetchTracksError = (error) => {return {type: FETCH_TRACKS_ERROR, error};};

export const deleteTrack = (id) => {
    return async dispatch => {
        try {
            dispatch(deleteTrackRequest());
            await axiosApi.delete('/tracks/' + id);
            dispatch(deleteTrackSuccess());
        } catch (e) {
            dispatch(deleteTrackError(e));
        }
    };
};
export const publishTrack= (id) => {
    return async dispatch => {
        try {
            dispatch(publishTrackRequest());
            const artist = await axiosApi.post('/tracks/publish/' + id);
            dispatch(publishTrackSuccess(artist.data));
        } catch (e) {
            dispatch(publishTrackError(e));
        }
    };
};

export const createTrack = (tracksData) => {

    return async dispatch => {
        try {
            dispatch(createTrackRequest());
            await axiosApi.post('/tracks', tracksData);
            dispatch(createTrackSuccess());
            dispatch(push('/' + tracksData.artist + "/albums/" + tracksData.album + "/tracks"));
        } catch (error) {
            if (error.response) {
                dispatch(createTrackError(error.response.data));
            } else {
                dispatch(createTrackError({global: "Network error or no internet"}));
            }
        }
    };
};


export const getTracks = (albumId) => {
    return async dispatch => {
        try{
            dispatch(fetchTracksRequest());
            const response = await axiosApi.get('/tracks?album='+ albumId);
            const tracks = response.data;
            dispatch(fetchTracksSuccess(tracks));
        }
        catch (e) {
            dispatch(fetchTracksError(e));
        }
    };
};