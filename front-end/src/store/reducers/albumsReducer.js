import {
    CREATE_ALBUM_ERROR,
    CREATE_ALBUM_REQUEST,
    CREATE_ALBUM_SUCCESS,
    FETCH_ALBUM_BY_ID_ERROR,
    FETCH_ALBUM_BY_ID_REQUEST,
    FETCH_ALBUM_BY_ID_SUCCESS,
    FETCH_ALBUMS_ERROR,
    FETCH_ALBUMS_REQUEST,
    FETCH_ALBUMS_SUCCESS,
    PUBLISH_ALBUM_SUCCESS
} from "../actions/albumsActions";

const initialState = {
    albums: [],
    loading:false,
    error: null,
    album: null,
    createError: null,
};

const albumsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ALBUM_BY_ID_REQUEST:
            return {...state};
        case FETCH_ALBUM_BY_ID_SUCCESS:
            return {...state, album: action.album, error:null};
        case FETCH_ALBUM_BY_ID_ERROR:
            return {...state, error: action.error};

        case FETCH_ALBUMS_REQUEST:
            return {...state, loading: true};
        case FETCH_ALBUMS_SUCCESS:
            return {...state, albums: action.albums, error:null, loading: false};
        case FETCH_ALBUMS_ERROR:
            return {...state, error: action.error, loading: false};

        case CREATE_ALBUM_REQUEST:
            return {...state, loading: true};
        case CREATE_ALBUM_SUCCESS:
            return {...state, createError:null, loading: false};
        case CREATE_ALBUM_ERROR:
            return {...state, createError: action.error, loading: false};

        case PUBLISH_ALBUM_SUCCESS:
            const index = state.albums.findIndex(a=>a._id === action.album._id);
            let albums = [...state.albums];
            let album = {...albums[index]};
            album.isPublished = true;
            albums[index] = album;
            return {
                ...state,
                albums:albums};
        default:
            return state;
    }
};

export default albumsReducer;