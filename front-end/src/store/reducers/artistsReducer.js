import {
    CREATE_ARTIST_ERROR,
    CREATE_ARTIST_REQUEST,
    CREATE_ARTIST_SUCCESS,
    FETCH_ARTIST_BY_ID_ERROR,
    FETCH_ARTIST_BY_ID_REQUEST,
    FETCH_ARTIST_BY_ID_SUCCESS,
    FETCH_ARTISTS_ERROR,
    FETCH_ARTISTS_REQUEST,
    FETCH_ARTISTS_SUCCESS,
    PUBLISH_ARTIST_SUCCESS
} from "../actions/artistsActions";

const initialState = {
    artists: [],
    loading:false,
    error: null,
    artist:null,
    createError:null
};

const artistsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ARTIST_BY_ID_REQUEST:
            return {...state};
        case FETCH_ARTIST_BY_ID_SUCCESS:
            return {...state, artist: action.artist, error:null};
        case FETCH_ARTIST_BY_ID_ERROR:
            return {...state, error: action.error};

        case FETCH_ARTISTS_REQUEST:
            return {...state, loading: true};
        case FETCH_ARTISTS_SUCCESS:
            return {...state, artists: action.artists, error:null, loading: false};
        case FETCH_ARTISTS_ERROR:
            return {...state, error: action.error, loading: false};

        case CREATE_ARTIST_REQUEST:
            return {...state, loading: true};
        case CREATE_ARTIST_SUCCESS:
            return {...state, createError:null, loading: false};
        case CREATE_ARTIST_ERROR:
            return {...state, createError: action.error, loading: false};

        case PUBLISH_ARTIST_SUCCESS:
            const index = state.artists.findIndex(a=>a._id === action.artist._id);
            let artists = [...state.artists];
            let artist = {...artists[index]};
            artist.isPublished = true;
            artists[index] = artist;
            return {
                ...state,
                artists:artists};
                // artists: [...state.artists, state.artists[index]={...state.artists[index], isPublished:true}]};
        default:
            return state;
    }
};

export default artistsReducer;