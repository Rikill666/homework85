import {
    CREATE_TRACK_HISTORY_ERROR,
    CREATE_TRACK_HISTORY_REQUEST,
    CREATE_TRACK_HISTORY_SUCCESS,
    FETCH_TRACKS_HISTORY_ERROR,
    FETCH_TRACKS_HISTORY_REQUEST,
    FETCH_TRACKS_HISTORY_SUCCESS
} from "../actions/trackHistoriesActions";

const initialState = {
    tracks: [],
    historyLoading:false,
    historyError: null,
};

const trackHistoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_HISTORY_REQUEST:
            return {...state, historyLoading:true};
        case FETCH_TRACKS_HISTORY_SUCCESS:
            return {...state, tracks: action.tracks, historyError:null, historyLoading:false};
        case FETCH_TRACKS_HISTORY_ERROR:
            return {...state, historyError: action.error, historyLoading:false};

        case CREATE_TRACK_HISTORY_REQUEST:
            return {...state, historyLoading: true};
        case CREATE_TRACK_HISTORY_SUCCESS:
            return {...state, historyError:null, historyLoading: false};
        case CREATE_TRACK_HISTORY_ERROR:
            return {...state, historyError: action.error, historyLoading: false};
        default:
            return state;
    }
};

export default trackHistoriesReducer;