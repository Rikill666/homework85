import {
    CREATE_TRACK_ERROR,
    CREATE_TRACK_REQUEST,
    CREATE_TRACK_SUCCESS,
    FETCH_TRACKS_ERROR,
    FETCH_TRACKS_REQUEST,
    FETCH_TRACKS_SUCCESS,
    PUBLISH_TRACK_SUCCESS
} from "../actions/tracksActions";

const initialState = {
    tracks: [],
    loading:false,
    error: null,
    createError:null
};

const tracksReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_TRACKS_REQUEST:
            return {...state, loading: true};
        case FETCH_TRACKS_SUCCESS:
            return {...state, tracks: action.tracks, error:null, loading: false};
        case FETCH_TRACKS_ERROR:
            return {...state, error: action.error, loading: false};

        case CREATE_TRACK_REQUEST:
            return {...state, loading: true};
        case CREATE_TRACK_SUCCESS:
            return {...state, createError:null, loading: false};
        case CREATE_TRACK_ERROR:
            return {...state, createError: action.error, loading: false};

        case PUBLISH_TRACK_SUCCESS:
            const index = state.tracks.findIndex(a=>a._id === action.track._id);
            let tracks = [...state.tracks];
            let track = {...tracks[index]};
            track.isPublished = true;
            tracks[index] = track;
            return {
                ...state,
                tracks:tracks};
        default:
            return state;
    }
};

export default tracksReducer;